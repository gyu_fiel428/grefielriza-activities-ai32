let data1 = [69, 31];
let colors1 = ['#49A9EA', '36CAAB'];


let myChart1 = document.getElementById("myChart").getContext('2d');

let char1 = new Chart(myChart1, {
    type: 'doughnut',
    data: {
        labels: labels1,
        dataset: [ {
            data: data1,
            backgroundColor: colors1
        }]
    },
    options: {
        title: {
            text: "Popular Book Genres",
            display: true
        }
    }
})